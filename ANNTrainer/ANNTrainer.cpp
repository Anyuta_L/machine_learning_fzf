#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;
	///-------------------------------------------------------

	bool buf;
	string filepath = "../TrainingAndTestData/xor_data.txt";	// filepath - ���� � ��� � ����� � �������
	vector<vector<float>> inputs;		//inputs - ����� ��� ������ ������
	vector<vector<float>> outputs; //outputs - ����� ��� ������ �������

	buf = LoadData(filepath, inputs, outputs);
	if (buf == false)
	{
		//�� ������� ��������� ��������� ������ �� ����� 
		cout << "could not read training data from file" << endl;
	}
	else
	{
		cout << " successfully !!! " << endl;
	}

	//configuration - ������������ ��������� ����
	shared_ptr<NeuralNetwork> nn;
	vector <int> configuration;

	configuration.resize(4);
	configuration[0] = inputs[0].size();
	configuration[1] = 10;
	configuration[2] = 10;
	configuration[3] = outputs[0].size();
	nn = CreateNeuralNetwork(configuration);

	//������� ��������� ����
	float buf1;
	buf1 = nn->MakeTrain(inputs, outputs, 100000, 0.1, 0.1, false);

	//������� ���������� � ���� ��������� ���� 
	string buf2;
	buf2 = nn-> GetType();

	//��������� ��������� ��������� ���� � ��������� ����
	bool buf3;
	string filepath_save = "../TrainingAndTestData/nn_save.txt";		//filepath_save - ��� � ���� �� ����� � ������
	buf3 = nn-> Save(filepath_save);
	if (buf3 == false)
	{
		//�� ������� �������� ��������� ���� � ��������� ����
		cout << "failed to write a neural network into a text file" << endl;
	}
	else
	{
		cout << " successfully !!! " << endl;
	}
	//---------------------------------------------------------------------------------------------
	system("pause");
	return 0;
}