#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;

	//-----------------------------------------------------------------
	shared_ptr<NeuralNetwork> nn;
	nn = CreateNeuralNetwork();
	//��������� ��������� ���� �� �����.
	bool buf;
	string filepath = "../TrainingAndTestData/nn_save.txt";	//filepath - ��� � ���� �� ����� � ������
	buf = nn-> Load( filepath);
	if (buf == false)
	{
		//�� ������� ��������� ��������� ���� �� ����� 
		cout << "could not read neural network from file" << endl;
	}
	else
	{
		cout << " Successfully!!! Read the neural network from file" << endl;
	}

	//�������� ������ � ����� ����
	string network_type;		//��� ����
	network_type = nn->GetType();
	// ������ �������� ������ �� �����
	vector<vector<float>> inputs;		//����� ��� ������ ������
	vector<vector<float>> outputs;		// ����� ��� ������ �������
	string filepath_data = "../TrainingAndTestData/xor_data.txt";// ���� � ��� � ����� � �������
	bool buf1;
	buf1 = LoadData(filepath_data,inputs,outputs);
	if (buf1 == false)
	{
		//�� ������� ��������� �������� ������ �� �����
		cout << "could not read test data from file" << endl;
	}
	else
	{
		cout << " Successfully!!! Read the test data from the file" << endl;
	}
	
	/**��������������� ����� �� ��������� �����
	* @param input - ����, ����� ������ ��������������� ���������� �������� �� ������� ����
	* @param output -�����, ����� ������ ��������������� ���������� �������� � �������� ����
	ANNDLL_API virtual std::vector<float> Predict(std::vector<float> & input) = 0;
	*/
	/*vector<float>given_input;	//�������� ����
	vector<float> output_for_a_given_input;	//����� �� ��������� �����
	given_input.resize(2);
	given_input[0] = 0.0;
	given_input[1] = 0.0;
	output_for_a_given_input.resize(1);
	output_for_a_given_input = nn->Predict(given_input);*/

	vector<float> output_for_a_given_input;	//����� �� ��������� �����
	output_for_a_given_input.resize(1);
	cout << endl<<endl;
	for (int i = 0; i < inputs.size(); i++)
	{
		//������� ������
		cout << "input data:		" << endl;
		for (int j = 0; j < inputs[i].size(); j++)
		{
			cout << inputs[i][j] << endl;
		}
		//��������� ���������
		cout << "Expected Result:		" << endl;
		for (int j = 0; j < outputs[i].size(); j++)
		{
			cout<< outputs[i][j] << endl;
		}
			 
			//��������� ������ ����
			output_for_a_given_input = nn->Predict(inputs[i]);
			cout << "result of network's work:		" << output_for_a_given_input[0] << endl;
			cout << endl;
	}
	
	//-----------------------------------------------------------------

	system("pause");
	return 0;
}